-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 06-03-2019 a las 23:20:51
-- Versión del servidor: 10.1.29-MariaDB
-- Versión de PHP: 7.1.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `dbsaf2`
--
CREATE DATABASE IF NOT EXISTS `dbsaf2` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE `dbsaf2`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbcliente`
--

DROP TABLE IF EXISTS `tbcliente`;
CREATE TABLE `tbcliente` (
  `clienteid` int(10) NOT NULL,
  `clientecedula` varchar(20) CHARACTER SET latin1 DEFAULT NULL,
  `clientenombre` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `clienteapellido1` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `clienteapellido2` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `clientedireccion` varchar(200) CHARACTER SET latin1 NOT NULL,
  `clientecorreo` varchar(50) CHARACTER SET latin1 NOT NULL,
  `clientetelefono` varchar(12) CHARACTER SET latin1 DEFAULT NULL,
  `clientemedidor` int(30) NOT NULL,
  `clientecasas` varchar(50) CHARACTER SET latin1 NOT NULL,
  `clientetipo` varchar(50) CHARACTER SET latin1 NOT NULL,
  `clienteestado` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `tbcliente`
--

INSERT INTO `tbcliente` (`clienteid`, `clientecedula`, `clientenombre`, `clienteapellido1`, `clienteapellido2`, `clientedireccion`, `clientecorreo`, `clientetelefono`, `clientemedidor`, `clientecasas`, `clientetipo`, `clienteestado`) VALUES
(11322, '222222222', 'Prueba', 'Prueba', 'Prueba', 'Finca 2', '', '', 0, '0', '3', 3),
(11311, '', 'Jennifer', 'Ellis', 'Araya', 'Finca 2', '', '', 4447, '1', '2', 1),
(11149, '', 'Ingrid', 'García', 'Alemán', 'Finca 2', '', '', 4448, '2', '2', 1),
(11183, '', 'José', 'Garita', 'Rubí', 'Finca 2', '', '', 4451, '1', '2', 1),
(11156, '', 'Jean Carlos', 'Cedeño', 'Medrano', 'Finca 2', '', '', 4452, '1', '2', 1),
(11211, '', 'Dayana', 'Aguirre', 'Rojas', 'Finca 2', '', '', 4453, '1', '2', 1),
(11293, '', 'Shirley', 'Espinoza', 'Rubí', 'Finca 2', '', '', 4454, '1', '2', 1),
(11272, '', 'Angelica', 'Santamaría', 'AS', 'Finca 2', '', '', 5187, '1', '2', 1),
(11312, '', 'Suly', 'Ellis', 'Araya', 'Finca 2', '', '', 5189, '1', '2', 1),
(11155, '', 'Jaqueline', 'Trejos', 'Villagra', 'Finca 2', '', '', 5191, '1', '2', 1),
(11154, '', ' Kerlin', 'Herrera', 'Rivera', 'Finca 2', '', '', 5193, '1', '2', 1),
(11291, '', 'Guadalupe', 'Mora', 'Salazar', 'Finca 2', '', '', 7924, '1', '2', 1),
(11310, '', 'Marcos', 'Benavides', 'Porras', 'Finca 2', '', '', 16825, '1', '2', 1),
(11225, '', 'ADI', 'Finca dos', 'Comunal', 'Finca 2', '', '', 16826, '1', '2', 1),
(11285, '', 'Farina', 'Murillo', 'Castro', 'Finca 2', '', '', 16827, '1', '2', 1),
(11297, '', 'Ana', 'Montero', 'Venegas', 'Finca 2', '', '', 16828, '1', '2', 1),
(11264, '', 'Nely', 'Jiménez', 'Hernández', 'Finca 2', '', '', 16829, '1', '2', 1),
(11292, '', 'Cecilia', 'Mora', 'Salazar', 'Finca 2', '', '', 16830, '1', '2', 1),
(11286, '', 'Patricia', 'Marín', 'Cartín', 'Finca 2', '', '', 16831, '1', '2', 1),
(11274, '', 'Francisco', 'Rodríguez', 'Araya', 'Finca 2', '', '', 16832, '1', '1', 1),
(11199, '', 'Cecilia', 'Barboza', 'Alvarado', 'Finca 2', '', '', 16833, '1', '2', 1),
(11128, '', 'Carlos', 'Corella', 'Mora', 'Finca 2', '', '', 16834, '1', '2', 1),
(11262, '', 'Efrain', 'Camacho', 'Hernández', 'Finca 2', '', '', 35577, '1', '2', 1),
(11240, '', 'Luis', 'Cortéz', 'Rodríguez', 'Finca 2', '', '', 36906, '1', '2', 1),
(11283, '', 'Mariam', 'Quiros', 'Brenes', 'Finca 2', '', '', 36909, '1', '2', 1),
(11238, '', 'Eliza', 'Sandoval', 'Solna', 'Finca 2', '', '', 36913, '1', '2', 1),
(11294, '', 'Oscar', 'Chávez', 'Sibaja', 'Finca 2', '', '', 36914, '1', '2', 1),
(11266, '', 'Darling', 'Rodríguez', 'DR', 'Finca 2', '', '', 36915, '1', '2', 1),
(11220, '', 'Patronato', 'Escolar', 'Finca dos', 'Finca 2', '', '', 37916, '1', '2', 1),
(11267, '', 'Marta', 'Rodríguez', 'Araya', 'Finca 2', '', '', 37917, '1', '2', 1),
(11296, '', 'Daniel', 'Cordero', 'Granados', 'Finca 2', '', '', 37920, '2', '2', 1),
(11265, '', 'Odir', 'Botris', 'OB', 'Finca 2', '', '', 37921, '1', '2', 1),
(11190, '', 'Noylin', 'Sevilla', 'Jiménez', 'Finca 2', '', '', 38886, '1', '2', 1),
(11251, '', 'Dignora', 'González', 'Ruíz', 'Finca 2', '', '', 38888, '2', '2', 1),
(11212, '', 'Erika', 'Aguirre', 'Rojas', 'Finca 2', '', '', 38889, '1', '2', 1),
(11131, '', 'Senia', 'Urbina', 'Madrigal', 'Finca 2', '', '', 38890, '1', '2', 1),
(11224, '', 'Carmen', 'Porras', 'Araya', 'Finca 2', '', '', 49202, '1', '1', 1),
(11315, '', 'Keilyn', 'Aguilar', 'Jiménez', 'Finca 2', '', '', 93662, '1', '2', 1),
(11268, '', 'Adita', 'Fallas', 'Rodríguez', 'Finca 2', '', '', 103725, '1', '2', 1),
(11295, '', 'Belisa', 'Calderón', 'Fernández', 'Finca 2', '', '', 154450, '1', '2', 1),
(11290, '', 'Maritza', 'Mora', 'Salazar', 'Finca 2', '', '', 196796, '1', '2', 1),
(11269, '', 'Viviana', 'Fallas', 'Rodríguez', 'Finca 2', '', '', 196797, '1', '2', 1),
(11160, '', 'Rebeca', 'Brenes', 'Valverde', 'Finca 2', '', '', 196798, '1', '2', 1),
(11161, '', 'Mayra', 'Cartín ', 'Araya', 'Finca 2', '', '', 196799, '1', '2', 1),
(11289, '', 'María Eugenia', 'Marín', 'Morales', 'Finca 2', '', '', 246268, '1', '2', 1),
(11118, '', 'Marcos', 'Durán', 'Alfaro', 'Finca 2', '', '', 247156, '1', '2', 1),
(11129, '', 'Oldemar', 'Salas', 'Monge', 'Finca 2', '', '', 247157, '1', '2', 1),
(11121, '', 'Álvaro', 'Muñoz', 'Martínez', 'Finca 2', '', '', 247159, '1', '2', 1),
(11233, '', 'Minor', 'Mesén', 'Mora', 'Finca 2', '', '', 247160, '2', '2', 1),
(11230, '', 'Alvaro', 'Castro', 'Segura', 'Finca 2', '', '', 247161, '2', '2', 1),
(11229, '', 'Cristina', 'Castro', 'Segura', 'Finca 2', '', '', 247162, '1', '2', 1),
(11124, '', 'Elieth', 'Medrano', 'Rodríguez', 'Finca 2', '', '', 247163, '1', '2', 1),
(11114, '', 'Mayra', 'Cartín', 'Araya', 'Finca 2', '', '', 247164, '1', '2', 1),
(11113, '', 'Carlos', 'Sibaja', 'Madrigal', 'Finca 2', '', '', 247165, '1', '2', 1),
(11321, '', 'Ramón', 'Gónzalez', 'Sánchez', 'Finca 2', '', '', 247166, '1', '2', 1),
(11178, '', 'María', 'Araya', 'Agüero', 'Finca 2', '', '', 247167, '1', '1', 1),
(11299, '', 'José Luis', 'Rodríguez', 'Castillo', 'Finca 2', '', '', 247168, '1', '2', 1),
(11256, '', 'Sara María', 'López', 'Carrillo', 'Finca 2', '', '', 247169, '1', '2', 1),
(11258, '', 'Alba', 'Trejos', 'Córtez', 'Finca 2', '', '', 247170, '1', '2', 1),
(11300, '', 'Virginia', 'López', 'Calderón', 'Finca 2', '', '', 247171, '1', '2', 1),
(11303, '', 'Asociación', 'de ', 'Mujeres', 'Finca 2', '', '', 247172, '1', '2', 1),
(11232, '111111111', 'Junta', 'de ', 'Educación (casa 1)', 'Finca 2', 'test@gmail.com', '11111111', 247173, '1', '2', 1),
(11306, '', 'Araceli', 'Sandí', 'Alvarez', 'Finca 2', '', '', 247175, '1', '2', 1),
(11191, '', 'Roxinia', 'Durán', 'Alfaro', 'Finca 2', '', '', 247176, '1', '2', 1),
(11308, '', 'Ismael', 'Soto', 'Jiménez', 'Finca 2', '', '', 247177, '1', '2', 1),
(11307, '', 'Claudio', 'Barrantes', 'Hernández', 'Finca 2', '', '', 247178, '2', '2', 1),
(11176, '', 'Flor', 'Gómez', 'Vargas', 'Finca 2', '', '', 247179, '1', '1', 1),
(11305, '', 'Dunia', 'Jiménez', 'Mayorga', 'Finca 2', '', '', 247180, '2', '1', 1),
(11277, '', 'Catalina', 'Rubí', 'Acuña', 'Finca 2', '', '', 247181, '1', '2', 1),
(11223, '', 'Carmen', 'Porras', 'Araya', 'Finca 2', '', '', 247182, '1', '2', 1),
(11228, '', 'Oficina', 'de', 'Salud', 'Finca 2', '', '', 247183, '1', '2', 1),
(11301, '', 'María Eugenia', 'Jiménez', 'Marín', 'Finca 2', '', '', 247184, '1', '2', 1),
(11231, '111111111', 'Junta', 'de ', 'Educación (casa 2)', 'Finca 2', 'test@gmail.com', '11111111', 247185, '1', '2', 1),
(11164, '', 'Juan', 'Sequeira', 'Pizarro', 'Finca 2', '', '', 247186, '1', '2', 2),
(11122, '', 'Victor', 'Araya', 'Padilla', 'Finca 2', '', '', 247187, '1', '2', 1),
(11119, '', 'Miguel', 'Carmona', 'Abarca', 'Finca 2', '', '', 247188, '1', '2', 1),
(11236, '', 'Alberto', 'Aguirre', 'Quesada', 'Finca 2', '', '', 247189, '1', '2', 1),
(11237, '', 'Luis', 'Cortéz', 'Rodríguez', 'Finca 2', '', '', 247190, '1', '2', 1),
(11170, '', 'Edwin', 'Araya', 'Mora', 'Finca 2', '', '', 247191, '1', '2', 1),
(11123, '', 'Juana', 'Castro', 'Aguirre', 'Finca 2', '', '', 247193, '1', '2', 1),
(11117, '', 'Carlos', 'Sibaja', 'Madrigal', 'Finca 2', '', '', 247194, '1', '1', 1),
(11125, '', 'Roxinia', 'Cortéz', 'Cortéz', 'Finca 2', '', '', 247195, '1', '2', 1),
(11213, '', 'Carlos', 'Fernández', 'Arias', 'Finca 2', '', '', 247196, '1', '1', 1),
(11151, '', 'Ilce', 'Porras', 'González', 'Finca 2', '', '', 247197, '1', '2', 1),
(11207, '', 'Jorge', 'Sánchez', 'Segura', 'Finca 2', '', '', 247200, '2', '1', 1),
(11201, '', 'Ana Melida', 'Fallas', 'Rodríguez', 'Finca 2', '', '', 247201, '1', '2', 1),
(11197, '', 'David', 'Rodríguez', 'Chavarría', 'Finca 2', '', '', 247202, '1', '2', 1),
(11193, '', 'Janet', 'Matarrita', 'Rangen', 'Finca 2', '', '', 247203, '1', '2', 1),
(11111, '', 'María', 'Andrade', 'Villalobos', 'Finca 2', 'andrade.axel.21@gmail.com', '', 247204, '1', '2', 1),
(11202, '', 'Ramón', 'Agüero', 'Agüero', 'Finca 2', '', '', 247206, '1', '2', 1),
(11188, '', 'Vanessa', 'Rodríguez', 'Valerio', 'Finca 2', '', '', 247207, '1', '2', 1),
(11139, '', 'Alejandrina', 'Rodríguez ', 'Reyes', 'Finca 2', '', '', 247208, '1', '2', 1),
(11157, '', 'Flor', 'Gómez', 'Vargas', 'Finca 2', '', '', 247209, '1', '2', 1),
(11194, '', 'Nelly', 'Chávez', 'Calvo', 'Finca 2', '', '', 247210, '1', '2', 1),
(11210, '', 'Damaris', 'Bogantes', 'Badilla', 'Finca 2', '', '', 247211, '1', '2', 1),
(11203, '', 'Marvin', 'González', 'Loría', 'Finca 2', '', '', 247212, '1', '2', 1),
(11195, '', 'Manuel', 'Quintanilla', 'Romero', 'F', '', '', 247213, '1', '2', 1),
(11214, '', 'Katia', 'Zuñiga', 'Victor', 'Finca 2', '', '', 247214, '1', '2', 1),
(11309, '', 'Juaquin', 'Camacho', 'JC', 'Finca 2', '', '', 247215, '1', '2', 1),
(11204, '', 'Roger', 'Murillo', 'Villegas', 'Finca 2', '', '', 247216, '1', '2', 1),
(11181, '', 'Nelly', 'Sánchez ', 'Oses', 'Finca 2', '', '', 247217, '1', '2', 1),
(11180, '', 'Marcos', 'Guerrero', 'Paniagüa', 'Finca 2', '', '', 247218, '1', '2', 1),
(11134, '', 'Catalina', 'Villagra', 'Gómez', 'Finca 2', '', '', 247219, '1', '2', 1),
(11132, '', 'Ana Lidia', 'Trejos', 'Trejos', 'Finca 2', '', '', 247221, '1', '2', 1),
(11185, '', 'Yadira', 'Jiménez', 'Jiménez', 'Finca 2', '', '', 247222, '1', '2', 1),
(11198, '', 'Eduviges', 'Carrillo', 'Sánchez', 'Finca 2', '', '', 247223, '1', '2', 1),
(11215, '', 'Maritza', 'López', 'Araya', 'Finca 2', '', '', 247224, '1', '2', 1),
(11208, '', 'Aura Estela', 'Espinoza', 'Blandón', 'Finca 2', '', '', 247225, '1', '2', 2),
(11205, '', 'William', 'Morera', 'Ramírez', 'Finca 2', '', '', 247226, '1', '2', 1),
(11144, '', 'Bernarda', 'Fernández', 'Granados', 'Finca 2', '', '', 247227, '1', '2', 1),
(11166, '', 'Juan', 'Jiménez', 'García', 'Finca 2', '', '', 247229, '1', '2', 1),
(11159, '', 'Sulay', 'Villalobos', 'Salas', 'Finca 2', '', '', 247230, '1', '2', 1),
(11281, '', 'Olga ', 'Campos', 'Carranza', 'Finca 2', '', '', 247231, '1', '2', 1),
(11140, '', 'Manuel', 'Abarca', 'Carmona', 'Finca 2', '', '', 247232, '1', '2', 1),
(11263, '', 'Ana', 'Yancy', 'Camacho', 'Finca 2', '', '', 247234, '1', '2', 1),
(11146, '', 'Victor', 'Vargas', 'Rojas', 'Finca 2', '', '', 247235, '1', '2', 1),
(11173, '', 'Martín', 'Victor', 'Tenorio', 'Finca 2', '', '', 247236, '1', '2', 1),
(11145, '', 'Doris', 'Muñoz', 'Martínez', 'Finca 2', '', '', 247237, '1', '2', 1),
(11172, '', 'Cecilia', 'Castro', 'Aguirre', 'Finca 2', '', '', 247238, '1', '2', 1),
(11174, '', 'Gerardo', 'Mosquera', 'Álvarez', 'Finca 2', '', '', 247239, '1', '2', 2),
(11167, '', 'Marina', 'Sandoval', 'Solano', 'Finca 2', '', '', 247240, '1', '2', 1),
(11141, '', 'Juan', 'Vargas', 'Rojas', 'Finca 2', '', '', 247241, '1', '1', 1),
(11184, '', 'Virginia', 'Rodríguez', 'Rodríguez', 'Finca 2', '', '', 247242, '1', '2', 1),
(11137, '', 'Edwin', 'Cedeño', 'Sanabria', 'Finca 2', '', '', 247243, '1', '1', 1),
(11126, '', 'Mario', 'Vega', 'Ramírez', 'Finca 2', '', '', 247244, '1', '2', 1),
(11162, '', 'Mayra', 'Cartín', 'Araya', 'Finca 2', '', '', 247245, '1', '2', 1),
(11182, '', 'Mayela', 'Rubí', 'Acuña', 'Finca 2', '', '', 247246, '1', '2', 1),
(11320, '111111111', 'Manuel', 'Araya', 'Calderón', 'Finca 2', 'test@gmail.com', '22222222', 247247, '1', '2', 1),
(11163, '', 'Rogelio', 'Mejías', 'Jiménez', 'Finca 2', '', '', 247248, '2', '2', 1),
(11206, '', 'Jorge', 'Sánchez', 'Segura', 'Finca 2', '', '', 247249, '1', '2', 1),
(11209, '', 'Marlene', 'Patiño', 'Martínez', 'Finca 2', '', '', 247250, '1', '2', 1),
(11136, '', 'Marian', 'García', 'Alemán', 'Finca 2', '', '', 247251, '1', '2', 1),
(11165, '', 'María', 'Zarate', 'Ávila', 'Finca 2', '', '', 247252, '1', '2', 1),
(11169, '', 'Xinia', 'Jiménez', 'Jiménez', 'Finca 2', '', '', 247253, '1', '2', 1),
(11171, '', 'Edwin', 'Araya', 'Mora', 'Finca 2', '', '', 247254, '1', '2', 1),
(11186, '', 'Miguel', 'López', 'Blanco', 'Finca 2', '', '', 247255, '1', '2', 1),
(11138, '', 'Ana Yancy', 'Castro', 'Segura', 'Finca 2', '', '', 247256, '1', '2', 1),
(11249, '', 'Margarita', 'Vega', 'Vega', 'Finca 2', '', '', 247257, '1', '2', 1),
(11143, '', 'Manuel', 'Espinoza', 'Espinoza', 'Finca 2', '', '', 247258, '1', '2', 1),
(11275, '', 'Alejo', 'Soto', 'Jiménez', 'Finca 2', '', '', 247259, '1', '2', 1),
(11302, '', 'Allan', 'Benavides', 'Porras', 'Finca 2', '', '', 247260, '1', '2', 1),
(11316, '', 'Madelina', 'Aguirre', 'Acevedo', 'Finca 2', '', '', 247261, '1', '2', 1),
(11135, '', 'Roger', 'Araya', 'Mora', 'Finca 2', '', '', 247262, '1', '2', 1),
(11284, '', 'Irian', 'Bogantes', 'Badilla', 'Finca 2', '', '', 247263, '1', '2', 1),
(11287, '', 'Dagoberto', 'Marín', 'Morales', 'Finca 2', '', '', 247265, '1', '2', 1),
(11234, '', 'Jorge', 'Montoya', 'Campos', 'Finca 2', '', '', 247266, '1', '2', 1),
(11112, '', 'María', 'Berrocal', 'Chinchilla', 'Finca 2', '', '', 247267, '1', '2', 1),
(11244, '', 'CEN', 'CINAI', 'ADEC', 'Finca 2', '', '', 247269, '1', '2', 1),
(11130, '', 'Margarita', 'López', 'Viales', 'Finca 2', '', '', 247270, '1', '2', 1),
(11243, '', 'EBAIS', 'Finca', 'Dos', 'Finca 2', '', '', 247272, '1', '2', 1),
(11246, '', 'Iglesia', 'Vida', 'Abundante', 'Finca 2', '', '', 247273, '1', '2', 1),
(11226, '', 'Marcos', 'Durán', 'Alfaro', 'Finca 2', '', '', 247274, '1', '2', 1),
(11227, '', 'ADI', 'Finca dos', 'Comunitaria', 'Finca 2', '', '', 247275, '1', '2', 2),
(11259, '', 'Rodolfo', 'Marín', 'Morales', 'Finca 2', '', '', 247284, '1', '2', 1),
(11239, '', 'Asociación ', 'de ', 'desarrollo', 'Finca 2', '', '', 247731, '1', '2', 1),
(11270, '', 'Irene', 'Rodríguez', 'Araya', 'Finca 2', '', '', 247732, '1', '2', 1),
(11273, '', 'Rodrigo', 'Rodríguez', 'Matamoros', 'Finca 2', '', '', 247733, '1', '2', 1),
(11248, '', 'Carlos', 'Loría', 'Mora', 'Finca 2', '', '', 247736, '1', '2', 1),
(11317, '', 'Flor', 'Rodríguez', 'Cordero', 'Finca 2', '', '', 247737, '1', '2', 1),
(11115, '', 'María', 'Madrigal', 'León', 'Finca 2', '', '', 247738, '1', '2', 1),
(11247, '', 'Catalina', 'Mora', 'CM', 'Finca 2', '', '', 247739, '1', '1', 1),
(11280, '', 'Junta', 'Edificadora', 'Catolica', 'Finca 2', '', '', 247740, '1', '2', 1),
(11221, '', 'Alberto', 'Benavides', 'Porras', 'Finca 2', '', '', 324701, '1', '2', 1),
(11147, '', 'Carlos', 'Guzmán', 'Vargas', 'Finca 2', '', '', 324704, '1', '2', 1),
(11242, '000000000', 'Alice', 'Zumbado', 'AZ', 'Finca 2', 'd@gmail.com', '22222222', 324710, '2', '1', 1),
(11260, '', 'Verania', 'Porras', 'Núñez', 'Finca 2', '', '', 325081, '1', '2', 1),
(11254, '', 'Kindri', 'Arias', 'López', 'Finca 2', '', '', 325218, '1', '2', 1),
(11276, '', 'Cintia', 'Espinoza', 'Rubi', 'Finca 2', '', '', 325584, '2', '2', 1),
(11235, '', 'Alcoholicos', 'Anónimos', 'AA', 'Finca 2', '', '', 325585, '1', '2', 2),
(11304, '', 'Jermy', 'Díaz', 'Delgado', 'Finca 2', '', '', 325586, '1', '2', 1),
(11179, '', 'Rebeca', 'Rodríguez', 'Arrieta', 'Finca 2', '', '', 325589, '1', '2', 1),
(11150, '', 'Ivania', 'Porras', 'Porras', 'Finca 2', '', '', 325694, '1', '2', 1),
(11279, '', 'Eduardo', 'Trigueros', 'Vega', 'Finca 2', '', '', 353455, '1', '2', 1),
(11282, '', 'Isaac', 'Ellis', 'Ellis', 'Finca 2', '', '', 353459, '1', '2', 1),
(11196, '', 'Rolando', 'Cordero', 'González', 'Finca 2', '', '', 355601, '1', '2', 1),
(11175, '', 'Dellanira', 'Araya', 'Venegas', 'Finca 2', '', '', 355602, '1', '2', 1),
(11217, '', 'Iris', 'Cartín', 'Araya', 'Finca 2', '', '', 355603, '1', '2', 1),
(11241, '', 'Blanca Nieves', 'Loría', 'Mora', 'Finca 2', '', '', 355604, '3', '1', 1),
(11120, '', 'Ronald', 'Cartín', 'Araya', 'Finca 2', '', '', 355605, '1', '2', 1),
(11245, '', 'Casa', 'Pastoral', 'CP', 'Finca 2', '', '', 355606, '1', '2', 1),
(11200, '', 'Gerardina', 'Jiménez', 'García', 'Finca 2', '', '', 355607, '1', '2', 1),
(11318, '', 'Selmira', 'Rodríguez', 'Chavarría', 'Finca 2', '', '', 355608, '1', '2', 1),
(11142, '', 'Juan Carlos', 'Guzmán', 'Vasquez', 'Finca 2', '', '', 355609, '1', '2', 1),
(11219, '', 'Edwin', 'Molina', 'Venegas', 'Finca 2', '', '', 355610, '1', '2', 1),
(11216, '', 'Kendall', 'Montoya', 'Calderón', 'Finca 2', '', '', 355733, '1', '2', 1),
(11222, '', 'Damaris', 'Benavides', 'Porras', 'Finca 2', '', '', 355739, '1', '2', 1),
(11257, '', 'Silvia', 'Nuñez', 'Morales', 'Finca 2', '', '', 355761, '1', '2', 1),
(11252, '', 'Maritza', 'Araya', 'Morales', 'Finca 2', '', '', 355762, '1', '2', 1),
(11298, '', 'Carmen', 'Venegas', 'Salas', 'Finca 2', '', '', 355764, '1', '2', 1),
(11152, '', 'Maribel', 'Madrigal', 'Flores', 'Finca 2', '', '', 355765, '1', '2', 1),
(11288, '', 'Dagoberto', 'Marín', 'Morales', 'Finca 2', '', '', 355766, '1', '2', 1),
(11153, '', 'Ofelia', 'Herrera', 'Rivera', 'Finca 2', '', '', 355767, '1', '2', 1),
(11278, '', 'Aura', 'Victor', 'Tenorio', 'Finca 2', '', '', 355768, '1', '2', 1),
(11133, '', 'Rebeca', 'Alemán', 'Ramírez', 'Finca 2', '', '', 355769, '1', '2', 1),
(11168, '', 'Antonio', 'Alfaro', 'Garro', 'Finca 2', '', '', 749201, '1', '2', 1),
(11189, '', 'Elba', 'Valerio', 'Chávez', 'Finca 2', '', '', 749205, '1', '2', 1),
(11319, '', 'Cecilio', 'Sánchez', 'Segura', 'Finca 2', '', '', 749209, '1', '2', 1),
(11127, '', 'Cecilia', 'Mora', 'Salazar', 'Finca 2', '', '', 783097, '1', '2', 1),
(11261, '', 'Deisy', 'Jiménez', 'Hernández', 'Finca 2', '', '', 783098, '1', '2', 1),
(11177, '', 'Flor', 'Gómez', 'Vargas', 'Finca 2', '', '', 783099, '1', '2', 1),
(11187, '111111111', 'Ramón', 'Alvarado', 'Alvarado', 'Finca 2', 'test@gmail.com', '11111111', 783100, '1', '2', 1),
(11255, '', 'Cristobal', 'Chávez', 'Mora', 'Finca 2', '', '', 783101, '1', '2', 1),
(11253, '', 'Jairo', 'Morera', 'Castro', 'Finca 2', '', '', 11103791, '1', '2', 1),
(11271, '', 'Luis', 'Rodríguez', 'Pérez', 'Finca 2', '', '', 11103888, '1', '2', 1),
(11218, '', 'Tatiana', 'Gómez', 'Cartín', 'Finca 2', '', '', 14074920, '1', '2', 1),
(11250, '', 'Luz Mery', 'Vega', 'Vega', 'Finca 2', '', '', 14074921, '1', '2', 1),
(11192, '', 'Junior', 'Trejos', 'Alfaro', 'Finca 2', '', '', 14749206, '1', '2', 1),
(11158, '', 'Flor', 'Gómez', 'Vargas', 'Finca 2', '', '', 15004449, '1', '1', 1),
(11313, '', 'Silvia', 'Benavides', 'Martínez', 'Finca 2', '', '', 111037922, '1', '2', 1),
(11148, '', 'William', 'García', 'García', 'Finca 2', '', '', 111037923, '1', '2', 2),
(11314, '', 'Eylin', 'Trigueros', 'Araya', 'Finca 2', '', '', 140749204, '1', '2', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbcobro`
--

DROP TABLE IF EXISTS `tbcobro`;
CREATE TABLE `tbcobro` (
  `cobroid` int(11) NOT NULL,
  `cobrofecha` varchar(20) NOT NULL,
  `cobroclienteid` int(11) NOT NULL,
  `cobroconcepto` int(11) NOT NULL,
  `cobroanio` int(11) NOT NULL,
  `cobromedidorid` int(11) NOT NULL,
  `cobrolecturaactual` int(11) NOT NULL,
  `cobrolecturaanterior` int(11) NOT NULL,
  `cobroconsumometroscubicos` int(11) NOT NULL,
  `cobrotarifabasica` int(11) NOT NULL,
  `cobrototalmetroscuadrados` int(11) NOT NULL,
  `cobroleyhidrante` int(11) NOT NULL,
  `cobrorecargo` int(11) NOT NULL,
  `cobrototalapagar` int(11) NOT NULL,
  `cobroestado` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbcobro`
--

INSERT INTO `tbcobro` (`cobroid`, `cobrofecha`, `cobroclienteid`, `cobroconcepto`, `cobroanio`, `cobromedidorid`, `cobrolecturaactual`, `cobrolecturaanterior`, `cobroconsumometroscubicos`, `cobrotarifabasica`, `cobrototalmetroscuadrados`, `cobroleyhidrante`, `cobrorecargo`, `cobrototalapagar`, `cobroestado`) VALUES
(1, '06-01-2019', 1, 0, 0, 247204, 3108, 3075, 33, 3360, 8106, 495, 239, 12200, 2),
(2, '12-02-2019', 1, 0, 2019, 247204, 3221, 3108, 113, 3360, 41387, 1695, 0, 46442, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbconsumo`
--

DROP TABLE IF EXISTS `tbconsumo`;
CREATE TABLE `tbconsumo` (
  `consumoid` int(11) NOT NULL,
  `consumoclientemedidor` int(11) NOT NULL,
  `consumometroscubicos` int(11) NOT NULL,
  `consumofecha` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbconsumo`
--

INSERT INTO `tbconsumo` (`consumoid`, `consumoclientemedidor`, `consumometroscubicos`, `consumofecha`) VALUES
(1, 12345, 92, '2018-11-12');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbempleado`
--

DROP TABLE IF EXISTS `tbempleado`;
CREATE TABLE `tbempleado` (
  `empleadoid` int(12) NOT NULL,
  `empleadocedula` varchar(20) NOT NULL,
  `empleadonombre` varchar(20) NOT NULL,
  `empleadoapellido1` varchar(25) NOT NULL,
  `empleadoapellido2` varchar(25) NOT NULL,
  `empleadodireccion` varchar(50) NOT NULL,
  `empleadocorreo` varchar(40) NOT NULL,
  `empleadotelefono` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbempleado`
--

INSERT INTO `tbempleado` (`empleadoid`, `empleadocedula`, `empleadonombre`, `empleadoapellido1`, `empleadoapellido2`, `empleadodireccion`, `empleadocorreo`, `empleadotelefono`) VALUES
(1, '999999999', 'Admin', 'Admin', 'Admin', 'Address', 'saf2@test.com', '99999999'),
(2, '777777777', 'User', 'User', 'User', 'Address', 'user@test.com', '22222222'),
(3, '292992929', 'Axel', 'Andrade', 'Villalobos', 'das', 'villalobos.axel@yahoo.es', '22222222');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbestado`
--

DROP TABLE IF EXISTS `tbestado`;
CREATE TABLE `tbestado` (
  `estadoid` int(11) NOT NULL,
  `estadodescripcion` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbestado`
--

INSERT INTO `tbestado` (`estadoid`, `estadodescripcion`) VALUES
(1, 'Activo'),
(2, 'Suspendido'),
(3, 'Otros');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbimpuestofijo`
--

DROP TABLE IF EXISTS `tbimpuestofijo`;
CREATE TABLE `tbimpuestofijo` (
  `impuestofijoid` int(11) NOT NULL,
  `impuestodescripcion` varchar(20) NOT NULL,
  `impuestovalor` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbimpuestofijo`
--

INSERT INTO `tbimpuestofijo` (`impuestofijoid`, `impuestodescripcion`, `impuestovalor`) VALUES
(1, 'Tarifa básica', 3360),
(2, 'Recargo', 2),
(3, 'Hidrante', 15),
(4, 'Reconexion', 9900);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbmediciongeneral`
--

DROP TABLE IF EXISTS `tbmediciongeneral`;
CREATE TABLE `tbmediciongeneral` (
  `medicionid` int(11) NOT NULL,
  `medicionclientemedidorid` int(11) NOT NULL,
  `Enero` int(11) DEFAULT NULL,
  `Febrero` int(11) DEFAULT NULL,
  `Marzo` int(11) DEFAULT NULL,
  `Abril` int(11) DEFAULT NULL,
  `Mayo` int(11) DEFAULT NULL,
  `Junio` int(11) DEFAULT NULL,
  `Julio` int(11) DEFAULT NULL,
  `Agosto` int(11) DEFAULT NULL,
  `Septiembre` int(11) DEFAULT NULL,
  `Octubre` int(11) DEFAULT NULL,
  `Noviembre` int(11) DEFAULT NULL,
  `Diciembre` int(11) DEFAULT NULL,
  `AnioActual` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbmediciongeneral`
--

INSERT INTO `tbmediciongeneral` (`medicionid`, `medicionclientemedidorid`, `Enero`, `Febrero`, `Marzo`, `Abril`, `Mayo`, `Junio`, `Julio`, `Agosto`, `Septiembre`, `Octubre`, `Noviembre`, `Diciembre`, `AnioActual`) VALUES
(1, 4447, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(2, 4448, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(3, 4451, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(4, 4452, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(5, 4453, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(6, 4454, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(7, 5187, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(8, 5189, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(9, 5191, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(10, 5193, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(11, 7924, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(12, 16825, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(13, 16826, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(14, 16827, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(15, 16828, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(16, 16829, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(17, 16830, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(18, 16831, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(19, 16832, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(20, 16833, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(21, 16834, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(22, 35577, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(23, 36906, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(24, 36909, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(25, 36913, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(26, 36914, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(27, 36915, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(28, 37916, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(29, 37917, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(30, 37920, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(31, 37921, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(32, 38886, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(33, 38888, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(34, 38889, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(35, 38890, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(36, 49202, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(37, 93662, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(38, 103725, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(39, 154450, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(40, 196796, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(41, 196797, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(42, 196798, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(43, 196799, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(44, 246268, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(45, 247156, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(46, 247157, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(47, 247159, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(48, 247160, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(49, 247161, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(50, 247162, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(51, 247163, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(52, 247164, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(53, 247165, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(54, 247167, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(55, 247168, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(56, 247169, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(57, 247170, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(58, 247171, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(59, 247172, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(60, 247175, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(61, 247176, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(62, 247177, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(63, 247178, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(64, 247179, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(65, 247180, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(66, 247181, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(67, 247182, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(68, 247183, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(69, 247184, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(70, 247185, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(71, 247186, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(72, 247187, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(73, 247188, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(74, 247189, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(75, 247190, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(76, 247191, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(77, 247193, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(78, 247194, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(79, 247195, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(80, 247196, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(81, 247197, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(82, 247200, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(83, 247203, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(84, 247204, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(85, 247206, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(86, 247207, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(87, 247208, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(88, 247209, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(89, 247210, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(90, 247211, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(91, 247212, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(92, 247213, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(93, 247214, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(94, 247215, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(95, 247216, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(96, 247217, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(97, 247218, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(98, 247219, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(99, 247221, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(100, 247222, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(101, 247223, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(102, 247224, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(103, 247225, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(104, 247226, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(105, 247227, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(106, 247229, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(107, 247230, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(108, 247231, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(109, 247232, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(110, 247234, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(111, 247235, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(112, 247236, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(113, 247237, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(114, 247238, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(115, 247239, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(116, 247240, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(117, 247241, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(118, 247242, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(119, 247243, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(120, 247244, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(121, 247245, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(122, 247246, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(123, 749201, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(124, 247248, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(125, 247249, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(126, 247250, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(127, 247251, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(128, 247252, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(129, 247253, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(130, 247254, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(131, 247255, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(132, 247256, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(133, 247257, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(134, 247258, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(135, 247259, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(136, 247260, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(137, 247201, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(138, 247262, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(139, 247263, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(140, 247265, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(141, 247266, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(142, 247267, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(143, 247269, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(144, 247270, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(145, 247272, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(146, 247273, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(147, 247274, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(148, 247275, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(149, 247284, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(150, 247731, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(151, 247732, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(152, 247733, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(153, 247736, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(154, 247738, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(155, 247739, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(156, 247740, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(157, 324701, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(158, 324704, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(159, 324710, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(160, 325081, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(161, 325218, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(162, 325584, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(163, 325585, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(164, 325586, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(165, 325589, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(166, 325694, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(167, 353455, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(168, 353459, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(169, 355601, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(170, 355602, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(171, 355603, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(172, 355604, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(173, 355605, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(174, 355606, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(175, 355607, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(176, 247202, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(177, 355609, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(178, 355610, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(179, 355733, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(180, 355739, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(181, 355761, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(182, 355762, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(183, 247173, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(184, 355764, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(185, 355765, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(186, 355766, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(187, 355767, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(188, 355768, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(189, 355769, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(190, 749205, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(191, 783097, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(192, 783098, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(193, 783099, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(194, 783100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(195, 783101, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(196, 11103791, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(197, 11103888, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(198, 14074920, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(199, 14074921, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(200, 14749206, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(201, 15004449, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(202, 111037922, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(203, 111037923, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019),
(204, 140749204, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbmedidaestandar`
--

DROP TABLE IF EXISTS `tbmedidaestandar`;
CREATE TABLE `tbmedidaestandar` (
  `medidaestandarid` int(11) NOT NULL,
  `medidaestandarrango` varchar(11) NOT NULL,
  `medidaestandardomipre` varchar(11) NOT NULL,
  `medidaestandaremprego` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbmedidaestandar`
--

INSERT INTO `tbmedidaestandar` (`medidaestandarid`, `medidaestandarrango`, `medidaestandardomipre`, `medidaestandaremprego`) VALUES
(1, '1-10', '217', 326),
(2, '11-30', '250', 375),
(3, '31-60', '312', 469),
(4, '61-999', '469', 469);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbmultilogin`
--

DROP TABLE IF EXISTS `tbmultilogin`;
CREATE TABLE `tbmultilogin` (
  `multiloginid` int(11) NOT NULL,
  `multiloginempleadoid` int(11) NOT NULL,
  `multiloginpassword` varchar(10) NOT NULL,
  `multilogintipousuario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbmultilogin`
--

INSERT INTO `tbmultilogin` (`multiloginid`, `multiloginempleadoid`, `multiloginpassword`, `multilogintipousuario`) VALUES
(1, 1, 'admin', 1),
(2, 2, '1234user', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbprevista`
--

DROP TABLE IF EXISTS `tbprevista`;
CREATE TABLE `tbprevista` (
  `previstaid` int(11) NOT NULL,
  `previstaclienteid` int(11) NOT NULL,
  `previstasaldoanterior` int(11) DEFAULT NULL,
  `previstaabonoactual` int(11) DEFAULT NULL,
  `previstasaldoactual` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbprevista`
--

INSERT INTO `tbprevista` (`previstaid`, `previstaclienteid`, `previstasaldoanterior`, `previstaabonoactual`, `previstasaldoactual`) VALUES
(1, 11322, 98000, 0, 98000);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbtipocliente`
--

DROP TABLE IF EXISTS `tbtipocliente`;
CREATE TABLE `tbtipocliente` (
  `tipoclienteid` int(11) NOT NULL,
  `tipoclientenombre` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbtipocliente`
--

INSERT INTO `tbtipocliente` (`tipoclienteid`, `tipoclientenombre`) VALUES
(1, 'Emprego'),
(2, 'Domipre'),
(3, 'Prevista');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbtipousuario`
--

DROP TABLE IF EXISTS `tbtipousuario`;
CREATE TABLE `tbtipousuario` (
  `tipousuarioid` int(11) NOT NULL,
  `tipousuariodescripcion` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbtipousuario`
--

INSERT INTO `tbtipousuario` (`tipousuarioid`, `tipousuariodescripcion`) VALUES
(1, 'administrador'),
(2, 'empleado');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `tbcliente`
--
ALTER TABLE `tbcliente`
  ADD PRIMARY KEY (`clientemedidor`);

--
-- Indices de la tabla `tbcobro`
--
ALTER TABLE `tbcobro`
  ADD PRIMARY KEY (`cobroid`);

--
-- Indices de la tabla `tbconsumo`
--
ALTER TABLE `tbconsumo`
  ADD PRIMARY KEY (`consumoid`),
  ADD KEY `consumoclienteid` (`consumoclientemedidor`);

--
-- Indices de la tabla `tbempleado`
--
ALTER TABLE `tbempleado`
  ADD PRIMARY KEY (`empleadoid`);

--
-- Indices de la tabla `tbestado`
--
ALTER TABLE `tbestado`
  ADD PRIMARY KEY (`estadoid`);

--
-- Indices de la tabla `tbmediciongeneral`
--
ALTER TABLE `tbmediciongeneral`
  ADD PRIMARY KEY (`medicionid`),
  ADD KEY `medicionclientemedidorid` (`medicionclientemedidorid`);

--
-- Indices de la tabla `tbmultilogin`
--
ALTER TABLE `tbmultilogin`
  ADD PRIMARY KEY (`multiloginid`),
  ADD KEY `multiloginempleadoid` (`multiloginempleadoid`);

--
-- Indices de la tabla `tbtipocliente`
--
ALTER TABLE `tbtipocliente`
  ADD PRIMARY KEY (`tipoclienteid`);

--
-- Indices de la tabla `tbtipousuario`
--
ALTER TABLE `tbtipousuario`
  ADD PRIMARY KEY (`tipousuarioid`);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `tbmediciongeneral`
--
ALTER TABLE `tbmediciongeneral`
  ADD CONSTRAINT `tbmediciongeneral_ibfk_1` FOREIGN KEY (`medicionclientemedidorid`) REFERENCES `tbcliente` (`clientemedidor`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `tbmultilogin`
--
ALTER TABLE `tbmultilogin`
  ADD CONSTRAINT `tbmultilogin_ibfk_1` FOREIGN KEY (`multiloginempleadoid`) REFERENCES `tbempleado` (`empleadoid`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
