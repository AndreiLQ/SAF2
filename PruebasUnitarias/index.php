<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>Pruebas unitarias QUNIT - SAF2</title>
  <link rel="stylesheet" href="qunit-2.9.2.css">
</head>
<body>

  <div id="qunit"></div>
  <div id="qunit-fixture"></div>
  
  <script src="qunit-2.9.2.js"></script>
  <script src="jquery-3.3.1.min.js"></script>
  



<script>

     var value = 1;
     var insertar ;
     var eliminar;
     var actualizar;
     var insertarRegistroMedicion;
     var actualizarRegistroMedicionEnero;
     var actualizarRegistroMedicionFebrero; 
     var eliminarRegistroMedicion;
     var insertarCobro;
     $.ajax({
        url: 'pruebasajax.php',
        type: 'get',
        dataType: 'JSON',
        async:false,
        success: function(response){
        
        insertar = response[0];
        eliminar = response[1];
        actualizar = response[2];

        insertarRegistroMedicion = response[3];
        actualizarRegistroMedicionEnero = response[4];
        actualizarRegistroMedicionFebrero = response[5];
        insertarCobro = response[6];
        eliminarRegistroMedicion = response[7];

       

         

        }
    });

    QUnit.test( "Insertar cliente domipre/emprego", function( assert ) {

      assert.equal( value,  insertar, "El cliente se ha insertado correctamente." );

    });

    QUnit.test( "Actualizar cliente domipre/emprego", function( assert ) {
     
      assert.equal( value,  actualizar, "El cliente se ha actualizado correctamente." );

    });

     QUnit.test( "Insertar registro medicion a cliente domipre/emprego", function( assert ) {

      assert.equal( value,  insertarRegistroMedicion, "El registro se ha creado correctamente." );

    });

    QUnit.test( "Actualizar consumo de metros cúbicos en Enero a cliente domipre/emprego", function( assert ) {


      assert.equal( value,  actualizarRegistroMedicionEnero, "El registro se ha actualizado correctamente." );

    });

    QUnit.test( "Actualizar consumo de metros cúbicos en Febrero a cliente domipre/emprego", function( assert ) {

      assert.equal( value,  actualizarRegistroMedicionFebrero, "El registro se ha actualizado correctamente." );

    });

    QUnit.test( "Realizar cobro a cliente domipre/emprego", function( assert ) {

      assert.equal( value,  insertarCobro, "El cobro se ha actualizado correctamente." );
    });

    QUnit.test( "Eliminar registro medicion a cliente domipre/emprego", function( assert ) {

      assert.equal( value,  eliminarRegistroMedicion, "El registro se ha eliminado correctamente." );

    });

    QUnit.test( "Eliminar cliente domipre/emprego", function( assert ) {

      assert.equal( value,  eliminar, "El cliente se ha eliminado correctamente." );
      
    });

    
    

     
  </script>

  
</body>
</html>
