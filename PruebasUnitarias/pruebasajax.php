<?php
include 'data.php';






$insertarCliente = insertarCliente();
$insertarRegistroMedicion = insertarMedicion();
$actualizarRegistroMedicionEnero = actualizarMedicionEnero();
$actualizarRegistroMedicionFebrero = actualizarMedicionFebrero();
 $insertarCobro = insertarCobro();
$eliminarRegistroMedicion = eliminarMedicion();

$actualizarCliente = actualizarCliente();
$eliminarCliente = eliminarCliente();


echo json_encode(array($insertarCliente,$actualizarCliente, $eliminarCliente,$insertarRegistroMedicion,$actualizarRegistroMedicionEnero,$actualizarRegistroMedicionFebrero,$insertarCobro,$eliminarRegistroMedicion));

?>

<?php 
    /*CLIENTES*/
	function actualizarCliente(){

        $retorno = 0;
        $pdo = Database::conectar();
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        //DECLARAR OBJETO
       
       $update = 'UPDATE `tbcliente` SET `clientecedula`=:clientecedula,`clientenombre`=:clientenombre,`clienteapellido1`=:clienteapellido1,`clienteapellido2`=:clienteapellido2,`clientedireccion`=:clientedireccion,`clientecorreo`=:clientecorreo,`clientetelefono`=:clientetelefono,`clientemedidor`=:clientemedidor,`clientecasas`=:clientecasas,`clientetipo`=:clientetipo,`clienteestado`=:clienteestado WHERE `clienteid` =:clienteid';
           
            $cedula = 9999;
            $nomb = "Prueba1";
            $ape1 = "Prueba2";
            $ape2 = "Prueba3";
            $dire = "Direccion";
            $corr = "someemail@gmail.com";
            $tele = 9999;
            $medi = 9999;
            $casas = 9999;
            $est = 1;
            $tipo = 2;
            $idcli = 9999;

            $q = $pdo->prepare($update);
            $q -> bindParam(':clientecedula', $cedula, PDO::PARAM_STR);
            $q -> bindParam(':clientenombre',$nomb , PDO::PARAM_STR);
            $q -> bindParam(':clienteapellido1',$ape1 , PDO::PARAM_STR);
            $q -> bindParam(':clienteapellido2', $ape2, PDO::PARAM_STR);
            $q -> bindParam(':clientedireccion', $dire, PDO::PARAM_STR);
            $q -> bindParam(':clientecorreo',$corr , PDO::PARAM_STR);
            $q -> bindParam(':clientetelefono',$tele , PDO::PARAM_STR);
            $q -> bindParam(':clientemedidor',$medi , PDO::PARAM_STR);
            $q -> bindParam(':clientecasas',$casas , PDO::PARAM_STR);
            $q -> bindParam(':clientetipo',$tipo , PDO::PARAM_STR);
            $q -> bindParam(':clienteestado',$propi , PDO::PARAM_STR);
            $q -> bindParam(':clienteid',$idcli , PDO::PARAM_STR);
            $q -> execute();
    
        //SI AGREGO QUE RETORNE 1
        //DE LO CONTRARIO 0
        if( ($q->rowCount()>0)){
            $retorno = 1;
            return $retorno;
        }else{
            $retorno = 0;
            return $retorno;
        }
        

       return $retorno;
        

    }

	function eliminarCliente(){

        $retorno = 0;
        $pdo = Database::conectar();
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        //DECLARAR OBJETO
        /*$medicion = new Medicion();
        
        $medicion -> setAnio(2099);
        $medicion -> setMedicionID(9999);*/
        $id = 9999;
        //DECLARAR SENTENCIA SQL
       $sql = "DELETE FROM tbcliente WHERE clienteid = ?";
        $q = $pdo->prepare($sql);
        $q->execute(array($id));

    
        //SI AGREGO QUE RETORNE 1
        //DE LO CONTRARIO 0
        if( ($q->rowCount()>0)){
            $retorno = 1;
            return $retorno;
        }else{
            $retorno = 0;
            return $retorno;
        }
        

       return $retorno;
        

    }

	function insertarCliente(){

        $retorno = 0;
        $pdo = Database::conectar();
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        //DECLARAR OBJETO
        /*$medicion = new Medicion();
       
        $medicion -> setAnio(2099);
        $medicion -> setMedidorID(999999);*/
        //DECLARAR SENTENCIA SQL
        $sql = 'INSERT INTO `tbcliente`(`clienteid`, `clientecedula`, `clientenombre`, `clienteapellido1`, `clienteapellido2`, `clientedireccion`, `clientecorreo`, `clientetelefono`, `clientemedidor`, `clientecasas`, `clientetipo`, `clienteestado`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)';
        //AGREGAR
        $q = $pdo->prepare($sql);
        $q->execute(array(9999,9999, "Prueba", "Prueba","Prueba","Prueba","Prueba",9999,9999,9999,1,1));
        //SI AGREGO QUE RETORNE 1
        //DE LO CONTRARIO 0
        if( ($q->rowCount()>0)){
            $retorno = 1;
            return $retorno;
        }else{
            $retorno = 0;
            return $retorno;
        }
        

       return $retorno;
        

    }

    /*MEDICIONES*/
    function insertarMedicion(){

        $retorno = 0;
        $pdo = Database::conectar();
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        //DECLARAR OBJETO
        /*$medicion = new Medicion();
       
        $medicion -> setAnio(2099);
        $medicion -> setMedidorID(999999);*/
        //DECLARAR SENTENCIA SQL
        $sql = 'INSERT INTO `tbmediciongeneral`(`medicionid`, `medicionclientemedidorid` , `AnioActual`) VALUES (?,?,?)';
        //AGREGAR
        $q = $pdo->prepare($sql);
        $q->execute(array(9999,9999, 9999));
        //SI AGREGO QUE RETORNE 1
        //DE LO CONTRARIO 0
        if( ($q->rowCount()>0)){
            $retorno = 1;
            return $retorno;
        }else{
            $retorno = 0;
            return $retorno;
        }
        

       return $retorno;
        

    }

    function actualizarMedicionEnero(){

        $retorno = 0;
        $pdo = Database::conectar();
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        //DECLARAR OBJETO
       /* $medicion = new Medicion();
        
        $medicion -> setAnio(2099);
        $medicion -> setMedidorID(999999);*/
        //DECLARAR SENTENCIA SQL
        $update = 'UPDATE `tbmediciongeneral` SET `Enero`=:mes WHERE `medicionclientemedidorid` =:medidorid AND `AnioActual` =:anio AND medicionID =:medicionid';
        $metrosCubicos = 1000;
        $medidorID = 9999;
        $year = 9999;
        $medicionID = 9999;

        $q = $pdo->prepare($update);
        $q -> bindParam(':mes', $metrosCubicos, PDO::PARAM_STR);
        $q -> bindParam(':medidorid', $medidorID, PDO::PARAM_STR);
        $q -> bindParam(':anio', $year, PDO::PARAM_STR);
        $q -> bindParam(':medicionid', $medicionID, PDO::PARAM_STR);
        $q -> execute();

    
        //SI AGREGO QUE RETORNE 1
        //DE LO CONTRARIO 0
        if( ($q->rowCount()>0)){
            $retorno = 1;
            return $retorno;
        }else{
            $retorno = 0;
            return $retorno;
        }
        

       return $retorno;
        

    }

    function actualizarMedicionFebrero(){

        $retorno = 0;
        $pdo = Database::conectar();
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        //DECLARAR OBJETO
       /* $medicion = new Medicion();
        
        $medicion -> setAnio(2099);
        $medicion -> setMedidorID(999999);*/
        //DECLARAR SENTENCIA SQL
        $update = 'UPDATE `tbmediciongeneral` SET `Febrero`=:mes WHERE `medicionclientemedidorid` =:medidorid AND `AnioActual` =:anio AND medicionID =:medicionid';
        $metrosCubicos = 1021;
        $medidorID = 9999;
        $year = 9999;
        $medicionID = 9999;

        $q = $pdo->prepare($update);
        $q -> bindParam(':mes', $metrosCubicos, PDO::PARAM_STR);
        $q -> bindParam(':medidorid', $medidorID, PDO::PARAM_STR);
        $q -> bindParam(':anio', $year, PDO::PARAM_STR);
        $q -> bindParam(':medicionid', $medicionID, PDO::PARAM_STR);
        $q -> execute();

    
        //SI AGREGO QUE RETORNE 1
        //DE LO CONTRARIO 0
        if( ($q->rowCount()>0)){
            $retorno = 1;
            return $retorno;
        }else{
            $retorno = 0;
            return $retorno;
        }
        

       return $retorno;
        

    }

    function eliminarMedicion(){

        $retorno = 0;
        $pdo = Database::conectar();
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        //DECLARAR OBJETO
        /*$medicion = new Medicion();
        
        $medicion -> setAnio(2099);
        $medicion -> setMedicionID(9999);*/
        $med = 9999;
        //DECLARAR SENTENCIA SQL
       $delete = 'DELETE FROM `tbmediciongeneral` WHERE `medicionid` =:medid';
        $q= $pdo->prepare($delete);
        $q->bindParam(':medid', $med, PDO::PARAM_INT);   
        $q->execute();

    
        //SI AGREGO QUE RETORNE 1
        //DE LO CONTRARIO 0
        if( ($q->rowCount()>0)){
            $retorno = 1;
            return $retorno;
        }else{
            $retorno = 0;
            return $retorno;
        }
        

       return $retorno;
        

    }


    function insertarCobro(){

            $retorno = 0;
            $pdo = Database::conectar();
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $consulta = 'INSERT INTO `tbcobro`(`cobroid`, `cobrofecha`, `cobroclienteid`, `cobroconcepto`, `cobroanio`, `cobromedidorid`, `cobrolecturaactual`, `cobrolecturaanterior`, `cobroconsumometroscubicos`, `cobrotarifabasica`, `cobrototalmetroscuadrados`, `cobroleyhidrante`, `cobrorecargo`, `cobrototalapagar`, `cobroestado`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';

            $stmt = $pdo ->prepare("SELECT MAX(cobroid) AS cobroid  FROM tbcobro");
            $stmt -> execute();

            $nextId = 1;
            
            if($row = $stmt->fetch()){
                $nextId = $row[0]+1;
            }

            $q = $pdo->prepare($consulta);
            $q->execute(array($nextId,'13-03-2019',9999,0,9999,9999,1000,1021,21,3360,4920,315,0,8595,2));
    
            if( ($q->rowCount()>0)){
            $retorno = 1;
            return $retorno;
        }else{
            $retorno = 0;
            return $retorno;
        }
        

       return $retorno;
            

               

            
            
           
        }



?>