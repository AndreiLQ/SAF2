<?php
 
	declare(strict_types=1);
 
	use PHPUnit\Framework\TestCase;
	use Calculadora;
	
	final class CalculadoraTest extends PHPUnit_Framework_TestCase{
		
		public function testSuma(){
			$calc = new Calculadora();
			$this->assertEquals(
			7,$calc -> sumar(3,4)
			);
		}
	}
